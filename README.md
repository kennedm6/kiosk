# kiosk

This repo contains the userscript js code for the infodesk kiosk.
Designed to run in chrome using the tampermonkey extension.

### Configuring

Create a new userscript in tampermonkey. Paste in the kiosk.js contents. Save. Go to the settings tab on the left. 
Add https://bitbucket.org/asvudev/kiosk/raw/HEAD/kiosk.js as the update url. Save. Go to the settings tab on the right.
Set the internal update interval to 6 hours. Save.

### Updating

New links can be added to the links array towards the top of the script, format ["LABEL", "https://urltogo.to"],

### Deploying

Edit and commit the new kiosk.js links in bitbucket. Tampermonkey should detect the changes and update the kiosk within 6 hours.