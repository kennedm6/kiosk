// ==UserScript==
// @name         VU Kiosk
// @namespace    https://vu.wwu.edu/
// @version      1.0
// @description  Kiosk menu wrapper
// @author       Me
// @match        *://*/*
// ==/UserScript==

(function() {
    'use strict';

    // kiosk link buttons ["LABEL", "https://urltogo.to"]
    var links = [
        ["MAP", "https://wwu.maps.arcgis.com/apps/webappviewer/index.html?id=365d9c1018724b42bf02eb915a7bd336"],
        ["LOST/FOUND", "http://lostfound.wwu.edu/lost/"],
        ["HOUSING", "https://housing.wwu.edu/"],
        ["JOBS", "https://vu.wwu.edu/StudentJobs"],
        ["EMPLOYMENT", "http://www.finaid.wwu.edu/studentjobs/"],
        ["WIN", "https://win.wwu.edu/"],
    ];

    var style = document.createElement("style");
    style.innerHTML = `
@import url('https://fonts.googleapis.com/css?family=Fira+Sans&display=swap');
body {
  margin-bottom: 120px;
}

#kiosk-menu {
  display: flex;
  justify-content: space-evenly;
  position: fixed;
  bottom: 0;
  left: 0;
  z-index: 9999999;
  width: 100%;
  height: 120px;
  background-color: #003f87;
  border-top: solid 4px white;
  font-family: Fira Sans,sans-serif !important;
  font-size: 22px !important;
}

.kiosk-button {
  display: flex;
  height: auto;
  background-color: #bad80a;
  border-radius: 200px;
  border: white solid 3px;
  color: #003f87;
  cursor: pointer;
  margin: 30px 0;
  padding: 15px 35px;
  vertical-align: middle;
  text-decoration: none;
  text-align: center;
  width: auto;
  line-height: 1;
}
`;
    document.head.appendChild(style);

    var menu = document.createElement("div");
    menu.id = "kiosk-menu";
    links.forEach(function(value, index, array) {
        var btn = document.createElement("div");
        btn.innerText = value[0];
        btn.className = "kiosk-button";
        btn.onclick = function() {window.location.href = value[1]};
        menu.appendChild(btn);
    });
    document.body.appendChild(menu);
})();